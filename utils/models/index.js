import ImagesModel from "../../entities/images";
import UserModel from "../../entities/user";

export async function syncModel() {
  try {
    UserModel.sequelize.sync();
    ImagesModel.sequelize.sync();
    ImagesModel.belongsTo(UserModel, {
      foreignKey: "UserId",
      as: "image",
    });
    UserModel.hasMany(ImagesModel, { as: "Owner" });
    console.log("All models were synchronized successfully.");
  } catch (e) {
    console.log("Sync models unsuccessful");
  }
}
