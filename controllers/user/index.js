import ImagesModel from "../../entities/images/index.js";
import UserModel from "../../entities/user/index.js";
import uniqid from "uniqid";

export async function getUserGallery(req, res) {
  try {
    const { email } = req.user;
    if (email !== req.params.user) {
      return res
        .status(400)
        .json({ message: "Неверная пара user-token, залогиньтесь заново" });
    }
    const user = await UserModel.findOne({
      where: { email },
      include: ["Owner"],
    });
    if (!user) {
      return res.status(400).json({ message: "Пользователь не найден" });
    }
    res.status(201).json(user);
  } catch (e) {
    console.log(e);
    res
      .status(500)
      .json({ message: "Не возможно получить  данные пользователя" });
  }
}
export async function setUserGallery(req, res) {
  try {
    const { email } = req.user;
    const user = await UserModel.findOne({
      where: { email },
    });
    if (!req.files) {
      return res.status(400).json({ message: "Нет ни одного файла" });
    }
    const { name, mimetype, data } = req.files.attachedFile;
    const newImage = {
      uniqId: uniqid(`${name}-`),
      fileName: name,
      fileType: mimetype,
      buffer: Buffer.from(data).toString("base64"),
      UserId: user.id,
    };
    await ImagesModel.create(newImage);

    res.status(200).json({ message: "Картинка загружена", newImage });
  } catch (e) {
    console.log(e.message);
    res.status(400).json({ message: "Что-то пошло не так" });
  }
}
export async function removeFromUserGallery(req, res) {
  try {
    console.log(req.body);
    const { uniqId } = req.body;
    if (!uniqId) {
      return res.status(400).json({ message: "Какая-то ошибких" });
    }
    const image = await ImagesModel.findOne({ where: { uniqId } });
    await image.destroy();

    res.status(200).json({ message: "Картинка удалена" });
  } catch (e) {
    console.log(e.message);
    res.status(400).json({ message: "Что-то пошло не так" });
  }
}
