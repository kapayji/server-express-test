import { DataTypes, Sequelize } from "sequelize";
const sequelize = new Sequelize(process.env.POSTGRES_SEQUELIZE_URI);
// const sequelize = new Sequelize(process.env.POSTGRES_SEQUELIZE_URI, {
//   dialect: "postgres",
//   dialectOptions: {
//     ssl: {
//       rejectUnauthorized: false,
//     },
//   },
// });

const ImagesModel = sequelize.define(
  "Images",
  {
    id: { type: DataTypes.INTEGER, autoIncrement: true, primaryKey: true },
    uniqId: { type: DataTypes.STRING, allowNull: false },
    fileName: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    fileType: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    buffer: {
      type: DataTypes.JSONB,
      allowNull: false,
    },
  },
  {
    freezeTableName: true,
  }
);

export default ImagesModel;
