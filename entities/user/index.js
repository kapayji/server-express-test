import { DataTypes, Sequelize } from "sequelize";
const sequelize = new Sequelize(process.env.POSTGRES_SEQUELIZE_URI);
// const sequelize = new Sequelize(process.env.POSTGRES_SEQUELIZE_URI, {
//   dialect: "postgres",
//   dialectOptions: {
//     ssl: {
//       rejectUnauthorized: false,
//     },
//   },
// });
const UserModel = sequelize.define(
  "User",
  {
    id: { type: DataTypes.INTEGER, autoIncrement: true, primaryKey: true },
    userName: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    email: {
      type: DataTypes.STRING,
      unique: true,
      allowNull: false,
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false,
    },
  },
  {
    freezeTableName: true,
  }
);

export default UserModel;
